﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RoslynSyntax
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string Source = @"
using System;

namespace Tekpoint.Api
{
    class Brand : PATApi
    {
        public void Method1()
        {

        }

    }
}
";
        private static string LastFile { get; set; }
        public MainWindow()
        {
            InitializeComponent();
        }

        private void loadFile_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = "CSharp|*.cs";
            dlg.FileName = LastFile??Directory.GetCurrentDirectory();
            if (dlg.ShowDialog() == true)
            {
                LastFile = dlg.FileName;
                LoadFile();
            }
        }

        private void LoadFile()
        {

            Cursor = Cursors.Wait;
            string content = LastFile == null? Source : File.ReadAllText(LastFile);
            sourceCode.Text = content;
            var syntaxTree = CSharpSyntaxTree.ParseText(content);
            var root = syntaxTree.GetCompilationUnitRoot();
            var newTree = CreateNewTree(root);
            generatedCode.Text = newTree.GetText().ToString();
            var items = GetTree(newTree, newTree);
            tree.Items.Clear();
            tree.Items.Add(items);
            Cursor = Cursors.Arrow;
        }

        private SyntaxNode CreateNewTree(SyntaxNode root)
        {
            var member = Find<MethodDeclarationSyntax>(root);
            var newRoot = Insert(root, member);
            return newRoot;
        }

        private SyntaxNode Find<T>(SyntaxNode current)
        {
            if (current.GetType() == typeof(T))
                return current;
            foreach (var child in current.ChildNodes())
            {
                var item = Find<T>(child);
                if (item != null)
                    return item;
            }
            return null;
        }

        private TreeViewItem GetTree(SyntaxNode root, SyntaxNode currentNode)
        {
            switch ((SyntaxKind)currentNode.RawKind)
            {
                case SyntaxKind.CompilationUnit:
                case SyntaxKind.UsingDirective:
                case SyntaxKind.NamespaceDeclaration:
                case SyntaxKind.ClassDeclaration:
                case SyntaxKind.MethodDeclaration:
                case SyntaxKind.PropertyDeclaration:
                case SyntaxKind.FieldDeclaration:
                case SyntaxKind.LocalFunctionStatement:

                    break;
                default:
                    return null;
            }


            var item = new TreeViewItem();
            string s = currentNode.Kind().ToString();
            if (currentNode is TypeDeclarationSyntax tds)
                s = s + " " + tds.Identifier.ValueText;
            if (currentNode is MethodDeclarationSyntax mds)
            {
                s = s + " " + mds.Identifier.ValueText;
                //if (s.Contains("ById"))
                   // Insert(root, currentNode);
            }
            if (currentNode is LocalFunctionStatementSyntax lfds)
                s = s + " : " + lfds.Identifier.ValueText;
            if (currentNode is PropertyDeclarationSyntax pds)
                s = s + " : " + pds.Identifier.ValueText;
            if (currentNode is FieldDeclarationSyntax fds)
                s = s + " : " + fds.ToString();// + fds.Declaration..ValueText;
            item.Header = s;// (root as TypeDeclarationSyntax)?.Identifier.ValueText ?? root.Kind().ToString();
            item.IsExpanded = true;
            //switch ((SyntaxKind)root.RawKind)
            //{
            //    case SyntaxKind.UsingDirective:
            //    case SyntaxKind.QualifiedName:
            //        item.Header = item.Header as string + root.GetText().ToString().Trim();
            //        return item;
            //}

            foreach (var child in currentNode.ChildNodes())
            {
                var childItem = GetTree(root, child);
                if (childItem != null)
                    item.Items.Add(childItem);
            }
            return item;
        }

        private static SyntaxNode Insert(SyntaxNode root, SyntaxNode node)
        {
            string content = @"
            public void MyMethod(int i = 3)
            {
                int j = i + 2;
                return;
            }
";
            string content1 = @"
            class A {
            public void MyMethod(int i = 3)
            {
                int j = i + 2;
                return;
            }
}
";
            var syntaxTree = CSharpSyntaxTree.ParseText(content, new CSharpParseOptions(kind: SourceCodeKind.Script));
            var newRoot = syntaxTree.GetCompilationUnitRoot();
            var firstChild = newRoot.ChildNodes().First();

            //var syntaxTree1 = CSharpSyntaxTree.ParseText(content1);
            //var method1 = syntaxTree1.GetCompilationUnitRoot().ChildNodes().First().ChildNodes().First();

            //var newTree1 = syntaxTree1.GetCompilationUnitRoot().ReplaceNode(method1, firstChild);
            return root.InsertNodesAfter(node, new []{firstChild});
        }

        private void scrollChanged(object sender, ScrollChangedEventArgs e)
        {
            sourceCode.ScrollToVerticalOffset(e.VerticalOffset);
        }

        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            LoadFile();
        }
    }
}
